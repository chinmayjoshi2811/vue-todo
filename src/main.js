import { createApp } from 'vue';
import App from './App.vue';
import BaseContainer from './components/UI/BaseContainer.vue';
import mainStore from './store';
import axios from 'axios';
import './index.css';

axios.defaults.baseURL = 'https://jsonplaceholder.typicode.com';

const app = createApp(App);
app.component('base-container', BaseContainer);
app.use(mainStore);
app.mount('#app');
