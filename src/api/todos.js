import axios from 'axios';

export const todoApi = {
  getTodos: async function (userId) {
    return await axios.get(`/users/${userId}/todos`);
  },
};
