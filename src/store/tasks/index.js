import mutations from './mutations';
import actions from './actions';
import getters from './getters';

// Create a new store instance.
const taskStore = {
  namespaced: true,
  state() {
    return {
      tasks: [],
    };
  },
  mutations,
  actions,
  getters,
};

export default taskStore;
