export default {
  setTasks(state, payload) {
    state.tasks = payload.data;
  },
  createTask(state, payload) {
    state.tasks.push(payload.data);
  },
  markDone(state, payload) {
    state.tasks = state.tasks.map((task) => {
      if (task.id === payload.id) {
        task.completed = true;
      }
      return task;
    });
  },
  deleteTask(state, payload) {
    state.tasks = state.tasks.filter((x) => x.id !== payload.id);
  },
};
