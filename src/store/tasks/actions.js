import { todoApi } from '../../api/todos';

export default {
  // async getTodos(context) {
  //   const { data } = await todoApi.getTodos(1);
  //   if (!data) return;
  //   context.commit('setTasks', { data });
  // },
  addTodo(context, payload) {
    const data = {
      id: new Date().toISOString(),
      ...payload,
    };
    context.commit('createTask', { data });
  },
  markDone(context, payload) {
    context.commit('markDone', payload);
  },
  deleteTask(context, payload) {
    context.commit('deleteTask', payload);
  },
};
