import { createStore } from 'vuex';
import taskStore from './tasks';
// Create a new store instance.
const store = createStore({
  modules: {
    taskModule: taskStore,
  },
  state() {
    return {
      userId: 1,
    };
  },
});

export default store;
